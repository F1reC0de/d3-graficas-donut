import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: []
})
export class PrincipalComponent implements OnInit {
  
  revenue:object;
  impresions:object;
  visits:object;

  constructor(private _dataService: DataService) { 

  }


  ngOnInit() {
    this.revenue = this._dataService.dataRevenue;
    this.impresions = this._dataService.dataImpresions;
    this.visits = this._dataService.dataVisits;

  }

}
