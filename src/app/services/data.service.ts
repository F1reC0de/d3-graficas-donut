import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  dataRevenue={
    
    titulo: 'REVENUE',
    areaColor:'#e3f0d8',
    lineaColor:'#c4d9b2',
    tabIzq:{ 
      titulo: 'Tablet',
      pct: 60,
      total: 120000,
      colorHexa:'#8bd03c',
      moneda:'€'
    },
    tabDerecho:{ 
      titulo: 'Smartphone',
      pct: 40,
      total: 80000,
      colorHexa:'#426415',
      moneda:'€'
    },
    dataLinearChart:[    
      {date: new Date('2010-01-01'), value: 700},
      {date: new Date('2010-01-04'), value: 655},
      {date: new Date('2010-01-08'), value: 630},
      {date: new Date('2010-01-09'), value: 607},
      {date: new Date('2010-01-10'), value: 609},
      {date: new Date('2010-01-11'), value: 580},
      {date: new Date('2010-01-12'), value: 600},
      {date: new Date('2010-01-16'), value: 450},
      {date: new Date('2010-01-19'), value: 400},
      {date: new Date('2010-01-20'), value: 320},
      {date: new Date('2010-01-21'), value: 330},
      {date: new Date('2010-01-22'), value: 240},
      {date: new Date('2010-01-23'), value: 320},
      {date: new Date('2010-01-24'), value: 200},
      {date: new Date('2010-01-25'), value: 100},
      {date: new Date('2010-01-26'), value: 1}
    ]

  }

  dataImpresions={
    titulo: 'IMPRESIONS',
    areaColor:'#d5e6ec',
    lineaColor:'#a8b9bf',
    tabIzq:{ 
      titulo: 'Tablet',
      pct: 40,
      total: 20000000,
      colorHexa:'#0bc6e3',
      moneda:''
    },
    tabDerecho:{ 
      titulo: 'Smartphone',
      pct: 60,
      total: 30000000,
      colorHexa:'#295264',
      moneda:''
    },
    dataLinearChart: [
      {date: new Date('2010-01-01'), value: 1},
      {date: new Date('2010-01-04'), value: 150},
      {date: new Date('2010-01-08'), value: 200},
      {date: new Date('2010-01-09'), value: 250},
      {date: new Date('2010-01-10'), value: 278},
      {date: new Date('2010-01-11'), value: 299},
      {date: new Date('2010-01-12'), value: 330},
      {date: new Date('2010-01-16'), value: 450},
      {date: new Date('2010-01-19'), value: 480},
      {date: new Date('2010-01-20'), value: 574},
      {date: new Date('2010-01-21'), value: 580},
      {date: new Date('2010-01-22'), value: 605},
      {date: new Date('2010-01-23'), value: 609},
      {date: new Date('2010-01-24'), value: 640},
      {date: new Date('2010-01-25'), value: 655},
      {date: new Date('2010-01-26'), value: 700},
    ]

  }

  dataVisits={
    titulo: 'VISITS',
    areaColor:'#FAFAEF',
    lineaColor:'#ECE6C9',
    tabIzq:{ 
      titulo: 'Tablet',
      pct: 80,
      total: 480000000,
      colorHexa:'#EFC12E',
      moneda:''
    },
    tabDerecho:{ 
      titulo: 'Smartphone',
      pct: 20,
      total: 120000000,
      colorHexa:'#BA551C',
      moneda:''
    },
    dataLinearChart: [
      {date: new Date('2010-01-01'), value: 800},
      {date: new Date('2010-01-04'), value: 630},
      {date: new Date('2010-01-08'), value: 580},
      {date: new Date('2010-01-09'), value: 570},
      {date: new Date('2010-01-10'), value: 590},
      {date: new Date('2010-01-11'), value: 560},
      {date: new Date('2010-01-12'), value: 570},
      {date: new Date('2010-01-16'), value: 470},
      {date: new Date('2010-01-19'), value: 480},
      {date: new Date('2010-01-20'), value: 480},
      {date: new Date('2010-01-21'), value: 478},
      {date: new Date('2010-01-22'), value: 460},
      {date: new Date('2010-01-23'), value: 470},
      {date: new Date('2010-01-24'), value: 490},
      {date: new Date('2010-01-25'), value: 489},
      {date: new Date('2010-01-26'), value: 1},
    ]

  }


  constructor() { }
}
