import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { DecimalPipe } from '@angular/common'
import * as d3 from 'd3-selection';
import * as d3Scale from 'd3-scale';
import * as d3Shape from 'd3-shape';
import * as d3Array from 'd3-array';
import * as d3Axis from 'd3-axis';


@Component({
  selector: 'app-grafica-compuesta',
  templateUrl: './grafica-compuesta.component.html',
  styleUrls: []
})
export class GraficaCompuestaComponent implements OnInit {

  @ViewChild('svgChart') private svgChart: ElementRef;
  
  @Input() intLineaColor='#ADD';
  @Input() intAreaColor='#AC9';
  @Input() intTitulo = 'FUNCIONAL';

  @Input() tabIzq ={ 
    titulo: 'Tablet',
    pct: 20,
    total: 400000,
    colorHexa:'#AC9',
    moneda:'€'
  }

  @Input() tabDerecho ={ 
    titulo: 'No se cual',
    pct: 80,
    total: 200000,
    colorHexa: '#ADD',
    moneda:'€'
  }

  @Input('dataLineChart') data = [
    {date: new Date('2010-01-01'), value: 700},
    {date: new Date('2010-01-04'), value: 655},
    {date: new Date('2010-01-08'), value: 630},
    {date: new Date('2010-01-09'), value: 607},
    {date: new Date('2010-01-10'), value: 609},
    {date: new Date('2010-01-11'), value: 580},
    {date: new Date('2010-01-12'), value: 600},
    {date: new Date('2010-01-16'), value: 450},
    {date: new Date('2010-01-19'), value: 400},
    {date: new Date('2010-01-20'), value: 320},
    {date: new Date('2010-01-21'), value: 330},
    {date: new Date('2010-01-22'), value: 240},
    {date: new Date('2010-01-23'), value: 320},
    {date: new Date('2010-01-24'), value: 200},
    {date: new Date('2010-01-25'), value: 100},
    {date: new Date('2010-01-26'), value: 1},
  ];

  private svg: any;
  private radioDonut = 130;
  private anchoLineaDonut = 10;
  private radioEspaciadoInterno= 8;




  // Se genera en ngOninit
  dataCircular = [];

  constructor(private decimalPipe: DecimalPipe) { 

  }

  ngOnInit() {

    console.log(this.intAreaColor);
    

    //Construccion de data circular
    this.dataCircular.push([0,this.tabDerecho.pct,this.tabDerecho.colorHexa]);
    this.dataCircular.push([this.tabDerecho.pct,this.tabIzq.pct + this.tabDerecho.pct ,this.tabIzq.colorHexa]);

    this.dataCircular.forEach((info)=>console.log(info));
    

    this.svg = d3.select(this.svgChart.nativeElement);
    this.generaTituloGrafica(this.intTitulo, 27, this.decimalPipe.transform(this.tabIzq.total +  this.tabDerecho.total)+ this.tabIzq.moneda,  40);
    this.generaClipPathCircular();
    this.generaGraficaDona(this.dataCircular);
    this.generaGraficaLineal(250,120, this.data, this.intAreaColor, this.intLineaColor);
  }

  /*====================================================*
    Genera el texto que se agregara a la grafica
   *====================================================*/ 
  generaTituloGrafica(titulo1, sizeTitulo1, titulo2, sizeTitulo2){
    this.svg.append('text')
    .text(titulo1)
        .attr('text-anchor', 'middle')
        .attr('font-size', sizeTitulo1+'px')
        .attr('font-family','arial')
        .style('fill', '#7d7d7d')
        .attr('transform','translate(130,100)');

    this.svg.append('text')
        .text(titulo2)
        .attr('text-anchor', 'middle')
        .attr('font-size', sizeTitulo2+'px')
        .attr('font-family','arial')
        .style('fill', '#18171c')
        .attr('transform','translate(130,145)');

  }


  /*====================================================*
    Genera el elemento clip-path para recorte de imagens
   *====================================================*/ 
  generaClipPathCircular(){

        this.svg.append('clipPath')
        .attr('id', 'ellipse-clip') // give the clipPath an ID 
        .append('ellipse')
        .attr('cx', this.radioDonut)
        .attr('cy',-30)
        .attr('rx', this.radioDonut - this.anchoLineaDonut - this.radioEspaciadoInterno )
        .attr('ry', this.radioDonut - this.anchoLineaDonut - this.radioEspaciadoInterno )
        // .attr('transform', `translate(${this.radioDonut},${this.radioDonut})`);
        
        // //
  }

  /*====================================================*
    Genera grafica de dona en el path principal
   *====================================================*/ 
  generaGraficaDona(data:any){
      // Se obtiene la escala para la grafica
      const cScale = d3Scale.scaleLinear()
      .domain([0, 100])
      .range([0, 2 * Math.PI]);

      // Se crea el arc para la grafica
      const arc:any = d3Shape.arc()
              .innerRadius(this.radioDonut-this.anchoLineaDonut)
              .outerRadius(this.radioDonut)
              .startAngle(function(d){return cScale(d[0])})
              .endAngle(function(d){return cScale(d[1])});

      // Se pinta la grafica 
      this.svg.selectAll('path')
      .data(data)
      .enter()
      .append('path')
      .attr('d', arc)
      .style('fill', function(d){return d[2];})
      .attr('transform', `translate(${this.radioDonut},${this.radioDonut})`);
  }

   /*====================================================*
    Genera la grafica lineal usando el clip path 'ellipse-clip'
    params:
      width: Ancho para la grafica
      height: Altura de la grafica
      data: Se informa con el arreglo de informacion 
      colorArea: Color para el area
      colorLinea: Color para la linea
   *====================================================*/ 
  generaGraficaLineal(width:number, height:number, data, colorArea:string, colorLinea:string){
    
      // Se obtienen las escalas
      const x = d3Scale.scaleTime().range([0, width]);
      const y = d3Scale.scaleLinear().range([height, 0]);
      x.domain(d3Array.extent(data, (d:any) => d.date ));
      y.domain(d3Array.extent(data, (d:any) => d.value ));            


      // Genera el area de la grafica
      var area = d3Shape.area()
      .curve(d3Shape.curveMonotoneX) // Curva el borde de la grafica
      .x((d: any) => x(d.date))
      .y0(height)
      .y1((d: any) => y(d.value));

      this.svg.append('path')
          .datum(data)
          .attr('fill',colorArea) 
          .attr('d', area)
          .attr('clip-path', 'url(#ellipse-clip)')
          .attr('transform',  `translate(0,  160)`  )


      // Generla la linea de la grafica
      let line = d3Shape.line()
          .curve(d3Shape.curveMonotoneX)
          .x( (d: any) => x(d.date) )
          .y( (d: any) => y(d.value) )

      this.svg.append('path')
          .datum(data)
          .attr('fill','none')
          .attr('stroke-width','4')
          .attr('stroke',colorLinea)
          .attr('d', line)
          .attr('clip-path', 'url(#ellipse-clip)')
          .attr('transform', `translate(0,  160)`)
  }
}
